---
layout: layouts/redirect
title: "Introduction to Android: Contexts, Intents, and the Activity lifecycle"
date: "2019-03-18"
description: "A basic overview of the main components of an Android app and how they interact with each other and the Android system."
redirect: https://unicorn-utterances.com/posts/introduction-to-android-framework/
link: https://unicorn-utterances.com/posts/introduction-to-android-framework/
tags: blog
---
