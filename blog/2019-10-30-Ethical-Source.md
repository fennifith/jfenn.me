---
layout: layouts/blog
title: "Ethical Source and Subjectivity"
description: "A mini-summary / explanation of some criticisms of the Ethical Source Movement."
date: "2019-10-30"
links:
  - name: "Ethical Source"
    icon: "https://ethicalsource.dev/images/logo.png"
    url: "https://ethicalsource.dev/"
  - name: "Open Source is Broken"
    url: "https://don.goodman-wilson.com/posts/open-source-is-broken/"
  - name: "Code of Code Ethics"
    icon: "https://wilkie.how/images/favicon.png"
    url: "https://wilkie.how/posts/code-of-code-ethics"
  - name: "Ethical Open Source: Is the world ready?"
    url: "https://www.torkinmanes.com/our-resources/publications-presentations/publication/ethical-open-source-is-the-world-ready"
  - name: "6 myths about \"ethical\" open source licenses"
    url: "https://hackernoon.com/6-myths-about-ethical-open-source-licenses-3bfbd042b1dc"
tags: blog
---

<div class="card card-accent"><p class="card-body">Many of the initial concerns described in this post have since been addressed - see the <a href="https://ethicalsource.dev/definition/">Ethical Source Definition</a> for updated information about the movement.</p></div>

<span class="text-large">The influence of ethics</span> and moral views in
open source software has recently seen somewhat of an increase in relevance and
discussion. Many people
[have tried to argue](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/30656)
that technology as a neutral tool that can/should be used by anyone regardless
of their political relevance. However, some members of the community argue that
this idea holds up poorly in actual use. Different people have different ethical
concerns, and while some ethics can be objective, they are often subjectively
biased towards the majority. This causes technology to frequently ignore the
cultural needs of its users in favor of its creators' view of the world, which
can fail to consider the real implications of the software on its users.

In response to this approach, Coraline Ada Ehmke - well known as the writer of
the Contributor Covenant - created the [Ethical Source Movement](https://ethicalsource.dev/).
Citing concerns that open source software is increasingly being taken advantage
of by fascist states and global corporations that treat workers unfairly, the
movement argues that the creators of a software should have the ability to
dictate its use by certain individuals and entities based on their ethical
values. Aside from this, the movement is similar to the open source definition,
placing emphasis on freely available source code and public contributions.

Ethical Source makes a lot of sense in the context that it is presented in. The
approach of traditional open source - to outright ban any form of
discrimination - avoids addressing any specific problem and separates the action
from the context in which it is used. It treats software as a neutral material -
like water or food - that should be available to everyone, regardless of their
beliefs. Simply making technology something that is written by and available for
absolutely anyone is an absurd and idealistic view which assumes that every
participant is objectively good. It assumes that everyone knows what they are
doing, and fails to account for any misuse or ignorance of these standards.

While the Ethical Source Movement addresses some fundamental issues of open
source, many critics have pointed out significant challenges of this approach.
The Open Source Initiative has failed to approve of the "ethical licenses" that
the movement involves, and many have attacked them as simplistic and
contradictory. While it clearly represents an improvement in some aspects of
open source, one might argue that it strays too far in the opposite direction
and subjects itself to the same fundamental problem. By establishing an unclear
viewpoint and relying on software creators to supply their ethical sense, it
tries to generalize a specific issue and loses some important context in the
process.

## Criticisms

One concern is that there is no clear way to realistically enforce such a policy
on any particular entity. It is unlikely that governments will disclose which
software they use and depend upon, and many creators of open source software
have no reasonable way to find out who it is being used by or how. Companies
that wrongly profit from open source are often found out because they, by
definition, redistribute the source material and violate the copyright law that
open source licenses are built upon. With internal applications, however, this
is impossible to detect without effectively removing the aspects of the software
that make it "open" and transparent. Any party that might take advantage of
freely available software to violate human rights will have no problem breaking
a license or two on top of that, and this fails to actually prevent their
operation in any way.

Moreso, such a broad term like "don't use this for evil please" in a license
agreement effectively passes the responsibility of judging whether something is
ethically sound to the user of the software. Even if it is used for evil, and
even if its existence and availability enabled that act, the creator didn’t
agree to it, which... somehow makes them no longer complicit in their actions?
It acts as shallow way to avoid any confrontation with an actual issue, and
ignore what the effect of providing such technology might actually be. It allows
creators to design their software without paying attention to the consequences
of its use because they no longer hold any liability for its actions; if
something happens, they can just point to the license terms and say "I told you
so" despite their moral failing to consider its actual effects.

Another issue with the definition of this movement is the assumption that the
values of a software’s creator are representative of the rest of society.
Allowing creators the ability to dictate how and when their software should be
used based on subjective ethical concerns assumes that they're a trustworthy
party to be given this power. Any creator that sees this as a justification to
deny service to an oppressive regime might just as easily turn it around to be
used as an excuse to deny service in far more subjective contexts. It opens the
door to using a different interpretation of ethics as an excuse to enforce
certain values upon users, and disproportionately gives power to the writers
and creators of technology.

## Effects

Despite the concerns of many critics, I don't think that the real value of this
movement is seen as a complete replacement of open source, and I don't know if
that's what it's really meant to be. Rather, the beneficial effect of this is in
its disruption to open source by bringing attention to these ethical issues and
challenging its actual definition. Regardless of whether such ideas are accepted
by the community, their impact spreads awareness that these issues exist, and
creates discussion that I believe results in a net benefit as everyone leaves
with a better understanding of the problems at hand.

Even with its potential flaws, Ethical Source is something I support because of
this, and because it raises an important question that open source has often
failed to address: _intent._

Open Source doesn't clearly define what an entity gains from their use of the
movement. It doesn't define what they bring to the community or what is taken
from it. And that marks a fairly substantial difference between open source
projects; while some developers approach it for moral reasons and a sense of
transparency, others simply see it as a way to share costs of development and
exploit free labor. Not that this is necessarily "bad" by itself, but it leaves
a lot of ambiguity in other aspects of open source that can't really be
described by a license - ownership, governance, incentive... Some people want to
build a community that can help them make decisions, others only want to provide
their work as a resource to learn from. These increasingly diverging ideas of
what open source actually is makes it difficult to universally define.

This is why I think that definitions of open source - especially the one
provided by the Open Source Initiative - should be treated more like guidelines
than an actual rulebook that everyone has to pedantically follow. When the OSI
wrote that OSS licenses must not discriminate against any people or groups, that
makes much more sense in one context than another - it makes sense to not
discriminate against people or groups in the definition of an "unjust or
prejudicial distinction". However, using that to argue that no distinctions
should be made at all is something that a good portion of the community would
have likely disagreed with.

And that brings me to a particular point: does open source have an absolute
meaning? More specifically, does it need to? This is the caveat of Ethical
Source, and what makes me a little conflicted about the direction it is going
in - it creates an entirely new term where I don't believe one is necessary, or
really representative of what the movement is trying to do. Is everyone going to
start calling it "ethical software" when they use a certain license? That's not
what "ethical" means. Simply using an "ethical license" means nothing if it
isn't adhered to.

Furthermore, some critics [have argued](https://subfictional.com/open-source-licenses-and-the-ethical-use-of-software/)
that in doing this, Ethical Source takes away from other efforts that could be
more effective and beneficial for the community. Encouraging people to abandon
open source and fracture the community seems like an unnecessary waste of the
effort that others have already put into the movement. Not that these aren't
significant issues in open source, but I would argue whether they actually need
a specific - and separate - terminology. Software movements aren't a species,
they're groups of humans with constantly changing opinions that would require
far more effort to define than what is necessary to derive them. Every single
individual developer can't have their own personal "open source movement" to
define what it means to them. This is reinventing subjectivity as an objective
construct.

The Open Source Initiative is normally very open to criticism of their practices
from the community, and as a result of that are usually a fairly accurate
representation of what it means. However, I think it's going a little far to say
that they are the sole arbiter of what can and cannot be a part of the movement.
People can work around that. Just because the OSI refuses to give your license
the official "open source badge of approval", does that make it completely
invalid? Of course not. It was made by a group of actual people with their own
thoughts and opinions that matter to them, whether the rest of the world agrees
or not.

## Solutions

In any situation, it's inevitably up to the community to decide what is right
and wrong, and creating a clearer ethical definition within that will solidify a
position on moral issues and possibly force the users of such tools to consider
their actions more deeply. This leads to better and more effective ways to
control them - through public debate and activism - than simply removing access
to a single piece of software in a sea of alternate options.

Of course, it's difficult for this to make any substantial change by itself. The
community can complain all they want; it's useless if no one is listening. And
this is where I defer to what someone else has thought of: organized groups to
leverage the power of individuals for a common purpose. Specifically, a
[software union](https://wilkie.how/posts/code-of-code-ethics). A space for
creators to act on their beliefs without treading on copyright or making
duplicate terminology.

The Ethical Source Movement is good, and I think it needs to exist - or, at
least, its values need to be written down as a thing that people agree with.
However, I think that representing it as a direct replacement for the open
source movement is inaccurate. It is a set of values, not something that any
software can arbitrarily conform to - and that could be better conveyed through
other means.
