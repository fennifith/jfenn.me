---
layout: layouts/redirect
title: "Minecraft Data Pack Programming: Introduction"
date: "2022-06-14"
description: "A beginner-friendly series for teaching programming concepts with Minecraft data packs."
redirect: https://unicorn-utterances.com/posts/minecraft-data-packs-introduction
link: https://unicorn-utterances.com/posts/minecraft-data-packs-introduction
tags: blog
---
