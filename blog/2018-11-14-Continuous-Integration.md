---
layout: layouts/redirect
title: Continuous Integration with Travis CI for Android
description: "An in-depth tutorial explaining how to set up Travis CI to deploy signed builds to Google Play. Among other things."
project: "ValCanBuild/TravisAndroidExample"
redirect: https://unicorn-utterances.com/posts/travis-ci-for-android/
link: https://unicorn-utterances.com/posts/travis-ci-for-android/
tags: blog
---
