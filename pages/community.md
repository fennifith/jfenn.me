---
layout: page
permalink: /community/
title: Community
---

<div class="text">

## Open Source

I like to publish the source code of my software because it allows other people to learn how it works and make their own improvements! It also reduces the workload on myself, allowing the community to answer their own questions and fix some problems without requiring my participation (or that of any particular individual).

For the most part, everyone working on these projects is motivated entirely by their own needs and desires. There is no professional support line to handle complaints or paid development team to fix critical bugs. Anyone that volunteers their time and expertise to improve my work does so without any obligation or commitment to the other users of the software.

### Contributing

I try to maintain my projects to meet the needs of all their users - as a result, most contributions will be accepted so long as they represent some kind of functional improvement. I'd much prefer to work together and resolve an issue than turn any genuine effort away. To that end, *if you need help with this process, have any questions or confusion, or want to get feedback before a contribution, please don't hesitate to [get in touch](/contact/)!*

For more specific contributing instructions, please see the README and/or CONTRIBUTING.md file in the repository.

## Community

I also manage a Discord server where participants can ask questions and discuss software and other current events.

<iframe src="https://discord.com/widget?id=514625116706177035&theme=dark" width="350" height="450" style="width: 100%; max-width: 350px;" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>

### Code of Conduct

Unless stated otherwise, all of my projects and spaces for discussion are subject to the guidelines of the [Contributor Covenant](https://www.contributor-covenant.org/). I believe that setting these expectations is necessary to create a safe and inclusive community.

Instances of behavior that violates this conduct may be reported to [conduct@jfenn.me](mailto:conduct@jfenn.me).

</div>
