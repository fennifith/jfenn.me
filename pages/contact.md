---
layout: page
permalink: /contact/
title: Contact
---

<div class="text">

# Contact Me

I have a [Discord server](https://discord.jfenn.me/) for general discussion about my projects. If you want to message me privately, you can contact me through any of the services below or send an email to [me@jfenn.me](mailto:me@jfenn.me).

To suggest improvements / report bugs in any of my projects, I would prefer that you create an issue in the project's repository (as it will save me time and allow others to enter the discussion), but feel free to contact me for any reason.

### Links

{% assign links = "Fediverse LinkedIn Telegram" | split: " " %}
{% for linkName in links %}
{% assign link = linkName | downcase %}
<a class="link link-chip" href="{{ meta.links[link] }}">
{% include "ic/{{ link }}.svg", class: "link-img" %} <span class="link-title">{{ linkName }}</span>
</a>
{% endfor %}

<div class="mt-5">
<iframe src="https://discord.com/widget?id=514625116706177035&theme=dark" width="350" height="450" style="width: 100%; max-width: 350px;" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
</div>

</div>
